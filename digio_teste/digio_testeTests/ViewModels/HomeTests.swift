//
//  HomeTests.swift
//  digio_testeTests
//
//  Created by Raphael Henrique on 29/10/20.
//

import XCTest

@testable import digio_teste

class HomeTests: XCTestCase {
    var homeViewModel: HomeViewModel!
    var mockHomeDataFetcher: MockHomeDataFetcher!
    
    override func setUp() {
        super.setUp()
        self.mockHomeDataFetcher = MockHomeDataFetcher()
        self.homeViewModel = HomeViewModel(provider: self.mockHomeDataFetcher)
    }
    
    override func tearDown() {
        super.tearDown()
        self.mockHomeDataFetcher = nil
        self.homeViewModel = nil
    }
}

class MockHomeDataFetcher {
    let homeModel: HomeModel
    init() {
        let spotlight = Spotlight(name: "Uber",
                                   bannerURL: "Teste abc",
                                   spotlightDescription: "Teste abc")
        let product = Product(name: "Xbox",
                               imageURL: "Teste abc",
                               productDescription: "Teste abc")
        let cash = Cash(title: "Xbox",
                         bannerURL: "Teste abc",
                         cashDescription: "Teste abc")
        self.homeModel = HomeModel(spotlight: [spotlight],
                                   products: [product],
                                   cash: cash)
    }
}

extension MockHomeDataFetcher: HomeProviderDelegate {
    func getHomeData(successCallBack: @escaping (HomeModel) -> Void, errorCallBack: @escaping (Error) -> Void) {
        successCallBack(self.homeModel)
    }
}

class CollectionViewTest: XCTestCase {
    var collectionView:UICollectionView!
    var data:[[String]] = [["First Section"], ["Second Section"]]
   
    override func setUp() {
        super.setUp()
       
        collectionView = UICollectionView(frame: CGRect(x: 0, y: 0, width: 100, height: 100), collectionViewLayout: UICollectionViewFlowLayout())
        collectionView.dataSource = self
       
        collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "cell")
    }
}

extension CollectionViewTest : UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return data.count
    }
   
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data[section].count
    }
   
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
    }
}
