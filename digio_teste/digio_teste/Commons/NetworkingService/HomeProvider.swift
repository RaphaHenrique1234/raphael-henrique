//
//  HomeProvider.swift
//  digio_teste
//
//  Created by Raphael Henrique on 26/10/20.
//

import Foundation

protocol HomeProviderDelegate {
    func getHomeData(successCallBack: @escaping (HomeModel) -> Void, errorCallBack: @escaping (Error) -> Void)
}

class HomeProvider: HomeProviderDelegate {
    func getHomeData(successCallBack: @escaping (HomeModel) -> Void, errorCallBack: @escaping (Error) -> Void) {
        
        guard Connectivity.isConnectedToInternet else {
            errorCallBack(ServiceError.noConnection)
            print(ServiceError.noConnection.localizedDescription)
            return
        }
        
        guard let url = URL(string: URLs.urlDigio) else {
            errorCallBack(ServiceError.noConnection)
            print(ServiceError.invalidURL.localizedDescription)
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let data = data else {
                errorCallBack(ServiceError.parsing)
                print(ServiceError.parsing.localizedDescription)
                return
            }
            
            do {
                let model = try JSONDecoder().decode(HomeModel.self, from: data)
                print("sucesso provider")
                print(model)
                successCallBack(model)
            } catch {
                errorCallBack(ServiceError.failure)
                print(ServiceError.failure.localizedDescription)
            }
        }
        .resume()
    }
}
