//
//  HomeViewModel.swift
//  digio_teste
//
//  Created by Raphael Henrique on 26/10/20.
//

import Foundation
import UIKit

protocol HomeViewModelDelegate: NSObject {
    func successResponse()
    func errorResponse(error: ServiceError)
}

class HomeViewModel {
    
    // MARK: - Class properties
    
    private var provider: HomeProviderDelegate
    
    // MARK: - Public properties
    
    weak var delegate: HomeViewModelDelegate?
    var data: HomeModel?
    var imagesSpotlight: [String] = []
    var imagesProduct: [String] = []
    var digioCash: Cash?
    var spotlight: [Spotlight] = []
    var product: [Product] = []
    
    // MARK: - Init methods
    
    init(provider: HomeProviderDelegate) {
        self.provider = provider
    }
    
    // MARK: - Class methods
    
    private func refreshContent(data: HomeModel) {
        self.data = data
        self.imagesSpotlight = data.spotlight?.compactMap({ $0.bannerURL }) ?? []
        self.spotlight = data.spotlight ?? []
        self.digioCash = data.cash
        self.imagesProduct = data.products?.compactMap({ $0.imageURL }) ?? []
        self.product = data.products ?? []
    }
    
    // MARK: - Public methods
    
    func fetchData() {
        self.provider.getHomeData(successCallBack: {[weak self] (data) in
            self?.refreshContent(data: data)
            self?.delegate?.successResponse()
        }) {[weak self] (error) in
            self?.delegate?.errorResponse(error: error as! ServiceError)
        }
    }
}
