//
//  SpotlightCollectionViewCell.swift
//  digio_teste
//
//  Created by Raphael Henrique on 26/10/20.
//

import UIKit
import Foundation

protocol SpotlightCollectionViewCellDelegate: class {
    func didClickSpotlight(name: String?)
}

class SpotlightCollectionViewCell: UICollectionViewCell {
    
    // MARK: - Outlets
    
    @IBOutlet weak var viewShadow: UIView!
    @IBOutlet weak var viewSpotlight: UIView!
    @IBOutlet weak var imageViewSpotlight: UIImageView!
    
    // MARK: - Class properties
    
    weak var delegate: SpotlightCollectionViewCellDelegate?
    private var nameSpotlight: String?
    
    // MARK: - Init methods
    
    override var reuseIdentifier: String {
        return SpotlightCollectionViewCell.className
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.backgroundColor = .clear
        self.viewSpotlight.backgroundColor = .white
        self.viewSpotlight.allCorner(cornerRadius: 15)
        self.viewShadow.shadowView()

        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(didClick))
        self.imageViewSpotlight.addGestureRecognizer(tapGesture)
    }
    
    @objc
    private func didClick() {
        self.delegate?.didClickSpotlight(name: self.nameSpotlight)
    }
    
    // MARK: - Public methods
    
    func passData(_ data: Spotlight) {
        self.nameSpotlight = data.name
        self.imageViewSpotlight.cacheImageSDWebImage(from: data.bannerURL, contentMode: .scaleAspectFill, completion: nil)
    }
}
