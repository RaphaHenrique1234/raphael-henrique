//
//  ProductsCollectionViewCell.swift
//  digio_teste
//
//  Created by Raphael Henrique on 26/10/20.
//

import UIKit

protocol ProductsCollectionViewCellDelegate: class {
    func didClickProduct(name: String?)
}

class ProductsCollectionViewCell: UICollectionViewCell {
    
    // MARK: - Outlets
    
    @IBOutlet weak var viewShadow: UIView!
    @IBOutlet weak var viewProduct: UIView!
    @IBOutlet weak var imageViewProduct: UIImageView!
    
    // MARK: - Class properties
    
    weak var delegate: ProductsCollectionViewCellDelegate?
    private var nameProduct: String?
    
    // MARK: - Init methods
    
    override var reuseIdentifier: String {
        return ProductsCollectionViewCell.className
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.backgroundColor = .clear
        self.viewProduct.backgroundColor = .white
        self.viewProduct.allCorner(cornerRadius: 15)
        self.viewShadow.shadowView()
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(didClick))
        self.viewProduct.addGestureRecognizer(tapGesture)
    }
    
    @objc
    private func didClick() {
        self.delegate?.didClickProduct(name: self.nameProduct)
    }
    
    // MARK: - Public methods
    
    func passData(_ data: Product) {
        self.nameProduct = data.name
        self.imageViewProduct.cacheImageSDWebImage(from: data.imageURL, contentMode: .scaleAspectFill, completion: nil)
    }
    
}
