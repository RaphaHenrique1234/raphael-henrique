//
//  HomeViewController.swift
//  digio_teste
//
//  Created by Raphael Henrique on 26/10/20.
//

import Foundation
import UIKit

class HomeViewController: UIViewController {
    
    // MARK: - Outlets
    
    @IBOutlet weak var viewContent: UIView!
    @IBOutlet weak var imageViewUser: UIImageView!
    @IBOutlet weak var labelNameUser: UILabel!
    @IBOutlet weak var collectionViewSpotlight: UICollectionView!
    @IBOutlet weak var labelDigioCash: UILabel!
    @IBOutlet weak var imageViewDigioCash: UIImageView!
    @IBOutlet weak var labelProducts: UILabel!
    @IBOutlet weak var collectionViewProducts: UICollectionView!
    @IBOutlet weak var labelError: UILabel!
    @IBOutlet weak var buttonError: UIButton!
    
    // MARK: - Class properties
    
    private enum Strings {
        static let labelNameUser = "Olá, Raphael Dev iOS GO.K"
        static let labelDigio = "digio"
        static let labelCash = "Cash"
        static let labelProduct = "Produtos"
        static let labelError = "Não foi possível atualizar a tela"
        static let textButtonError = "Atualizar"
        static let errorAlert = "Sem conexão à internet"
    }
    
    // MARK: - Public properties
    
    let viewModel: HomeViewModel
    
    // MARK: - Init methods
    
    init() {
        let provider = HomeProvider()
        self.viewModel = HomeViewModel(provider: provider)
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewModel.delegate = self
        self.setupCollectionsView()
        self.setupContentHome()
        self.viewModel.fetchData()
    }
    
    // MARK: - Class methods
    
    private func setupCollectionsView() {
        self.collectionViewSpotlight.delegate = self
        self.collectionViewSpotlight.dataSource = self
        self.collectionViewSpotlight.registerCell(SpotlightCollectionViewCell.className)
        self.collectionViewSpotlight.backgroundColor = .clear
    
        self.collectionViewProducts.delegate = self
        self.collectionViewProducts.dataSource = self
        self.collectionViewProducts.registerCell(ProductsCollectionViewCell.className)
        self.collectionViewProducts.backgroundColor = .clear
    }
    
    private func setupContentHome() {
        self.viewContent.isHidden = true
        self.view.backgroundColor = .white
        self.viewContent.backgroundColor = .clear
        self.imageViewDigioCash.allCorner(cornerRadius: 10)
        self.imageViewUser.layer.cornerRadius = 0.5 * imageViewUser.bounds.size.width
        self.imageViewUser.layer.borderWidth = 2
        self.imageViewUser.layer.borderColor = UIColor.black.cgColor
        self.imageViewUser.clipsToBounds = true
        
        let tapGestureImageDigioCash = UITapGestureRecognizer(target: self, action: #selector(didClickDigioCash))
        self.imageViewDigioCash.addGestureRecognizer(tapGestureImageDigioCash)
    }
    
    private func dataHome() {
        self.labelNameUser.text = Strings.labelNameUser
        self.imageViewUser.image = UIImage(named: "digioUser")
        self.formatTextLabelDigioCash()
        self.imageViewDigioCash.cacheImageSDWebImage(from: self.viewModel.digioCash?.bannerURL, contentMode: .scaleAspectFill, completion: nil)
        self.labelProducts.text = Strings.labelProduct
        self.labelError.isHidden = true
        self.buttonError.isHidden = true
        self.viewContent.isHidden = false
    }
    
    private func formatTextLabelDigioCash() {
        let textDigioCash = self.viewModel.digioCash?.title
        let attributedString = NSMutableAttributedString(string: textDigioCash ?? "")
        attributedString.addAttribute(.font, value: UIFont.systemFont(ofSize: 8), range: NSRange(location: 0, length: 2))
        attributedString.addAttribute(.font, value: UIFont.systemFont(ofSize: 12), range: NSRange(location: 0, length: 4))
        attributedString.addAttribute(.font, value: UIFont.systemFont(ofSize: 16), range: NSRange(location: 0, length: 6))
        self.labelDigioCash.attributedText = attributedString
    }
    
    private func showErrorScreen() {
        self.viewContent.isHidden = false
        self.labelError.text = Strings.labelError
        self.buttonError.setTitle(Strings.textButtonError, for: .normal)
        self.labelError.isHidden = false
        self.buttonError.isHidden = false
        self.labelNameUser.isHidden = true
        self.labelProducts.isHidden = true
        self.labelDigioCash.isHidden = true
        self.imageViewUser.isHidden = true
    }
    
    private func showSucessScreen() {
        self.viewContent.isHidden = false
        self.labelError.isHidden = true
        self.buttonError.isHidden = true
        self.labelNameUser.isHidden = false
        self.labelProducts.isHidden = false
        self.labelDigioCash.isHidden = false
        self.imageViewUser.isHidden = false
    }
    
    // MARK: - Action
    
    @objc
    private func didClickDigioCash() {
        if let nameScreen = self.viewModel.digioCash?.title {
            print("Ir para tela \(nameScreen)")
        }
    }
    
    @IBAction func buttonRefreshPressed(_ sender: Any) {
        self.viewModel.fetchData()
    }
    
}

// MARK: - Extensions

extension HomeViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.collectionViewSpotlight {
            return self.viewModel.imagesSpotlight.count
        } else {
            return self.viewModel.imagesProduct.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.collectionViewSpotlight {
            let cellSpotlight = self.collectionViewSpotlight.dequeueReusableCell(ofType: SpotlightCollectionViewCell.self, for: indexPath)
            
            cellSpotlight.passData(self.viewModel.spotlight[indexPath.row])
            cellSpotlight.delegate = self
            return cellSpotlight
            
        } else {
            let cellProducts = self.collectionViewProducts.dequeueReusableCell(ofType: ProductsCollectionViewCell.self, for: indexPath)
            
            cellProducts.passData(self.viewModel.product[indexPath.row])
            cellProducts.delegate = self
            return cellProducts
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == self.collectionViewSpotlight {
            return CGSize(width: self.collectionViewSpotlight.frame.width/1.3, height: self.collectionViewSpotlight.frame.height)
            
        } else {
            return CGSize(width: self.collectionViewProducts.frame.width/2.8, height: self.collectionViewProducts.frame.height)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == self.collectionViewSpotlight {
            return 16
        } else {
            return 16
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let borderLeftCollection = CGFloat(19) // 24 borda da tela, menos 5 da sombra esquerda
        let borderRightCollection = CGFloat(22) // 24 borda da tela, menos 2 da sombra direita
        
        if collectionView == self.collectionViewSpotlight {
            return UIEdgeInsets(top: 0,left: borderLeftCollection,bottom: 0,right: borderRightCollection)
            
        } else {
            return UIEdgeInsets(top: 0,left: borderLeftCollection,bottom: 0,right: borderRightCollection)
        }
    }
}

extension HomeViewController: SpotlightCollectionViewCellDelegate {
    func didClickSpotlight(name: String?) {
        if let nameScreen = name {
            print("Ir para tela \(nameScreen) do Spotlight")
        }
    }
}

extension HomeViewController: ProductsCollectionViewCellDelegate {
    func didClickProduct(name: String?) {
        if let nameScreen = name {
            print("Ir para tela \(nameScreen) do Produto")
        }
    }
    
}

extension HomeViewController: HomeViewModelDelegate {
    func successResponse() {
        DispatchQueue.main.async {
            self.dataHome()
            self.collectionViewSpotlight.reloadData()
            self.collectionViewProducts.reloadData()
            self.showSucessScreen()
        }
    }
    
    func errorResponse(error: ServiceError) {
        DispatchQueue.main.async {
            self.showErrorScreen()
            
            self.showAlertCommon(title: Strings.errorAlert, message: nil, handler: nil)
        }
    }
}
